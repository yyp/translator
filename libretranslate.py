import json
import requests
import config


def translate(text, to_language, from_language="auto"):
    r = requests.post(
        config.libretranslate_url,
        json={
            "q": text,
            "source": from_language,
            "target": to_language
        })

    #response = dict(r.text)
    response = json.loads(r.text)
    if "translatedText" in response:
        return response["translatedText"]
    elif "error" in response:
        return response["error"]
    else:
        return "odd, something went wrong"


if __name__ == "__main__":
    print(translate("lutsch meinen schwanz", "en"))
