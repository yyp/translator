import lxml.html as lxml
import requests

def translate(text, to_language, from_language="auto"):
    r = requests.get(
        "https://translate.google.com/m",
        params={
            "q": text,
            "sl": from_language,
            "tl": to_language
        })

    doc = lxml.fromstring(r.text)
    for container in doc.find_class("result-container"):
        return container.text_content()

    return ""

if __name__ == "__main__":
    print(translate("hello", "fr", "en"))
